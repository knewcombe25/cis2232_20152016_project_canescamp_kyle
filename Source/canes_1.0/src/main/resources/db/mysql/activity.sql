CREATE TABLE Activity
(
activityId int NOT NULL AUTO_INCREMENT,
name varchar(255),
description varchar(255),
rules varchar(255),
comments varchar(255),
rating int,
syncstamp timestamp,
PRIMARY KEY (activityId)
);


CREATE TABLE CampActivities
(
activityId int NOT NULL,
campId int NOT NULL,
startDate varchar(255),
startTime varchar(255),
PRIMARY KEY(campId, activityId),
FOREIGN KEY(activityId) REFERENCES Activity(activityId)
	ON UPDATE CASCADE ON DELETE CASCADE,	
FOREIGN KEY(campId) REFERENCES Camp(campId)
	ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO `Activity` (`activityId`, `name`, `description`, `rules`, `comments`, `rating`, `syncstamp` ) VALUES
(1, 'Activity 1', 'Activity 1 Description', 'Activity 1 Rules', 'Activity 1 Comments', 5, NOW() );

INSERT INTO `Activity` (`activityId`, `name`, `description`, `rules`, `comments`, `rating`, `syncstamp` ) VALUES
(2, 'Activity 2', 'Activity 2 Description', 'Activity 2 Rules', 'Activity 2 Comments', 5, NOW()  );

INSERT INTO `Activity` (`activityId`, `name`, `description`, `rules`, `comments`, `rating`, `syncstamp` ) VALUES
(3, 'Activity 3', 'Activity 3 Description', 'Activity 3 Rules', 'Activity 3 Comments', 5 , NOW() );

INSERT INTO `Camp` (`campId`, `description`, `descriptionLong`, `startDate`, `endDate`, `capacity`, `contactName`) VALUES
(1, 'Camp 1', 'Camp Description', '09/02/2016', '19/02/2016', 10, 'Santa');

INSERT INTO `Camp` (`campId`, `description`, `descriptionLong`, `startDate`, `endDate`, `capacity`, `contactName`) VALUES
(2, 'Camp 2', 'Camp Description', '09/02/2016', '19/02/2016', 10, 'Santa');

INSERT INTO `CampActivities` (`activityId`, `campId`, `startDate`, `startTime`) VALUES
(1, 1, '09/02/2016', '12:00' );

INSERT INTO `CampActivities` (`activityId`, `campId`, `startDate`, `startTime`) VALUES
(2, 1, '10/02/2016', '13:00' );

INSERT INTO `CampActivities` (`activityId`, `campId`, `startDate`, `startTime`) VALUES
(2, 2, '11/02/2016', '14:00' );

INSERT INTO `CampActivities` (`activityId`, `campId`, `startDate`, `startTime`) VALUES
(3, 2, '12/02/2016', '15:00' );
