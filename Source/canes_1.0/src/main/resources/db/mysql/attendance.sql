CREATE TABLE IF NOT EXISTS `Attendance` (
  `attendanceId` int(5) NOT NULL AUTO_INCREMENT COMMENT 'PK ',
  `checkedIn` BOOLEAN NOT NULL DEFAULT 0 COMMENT 'Registrant has checked in',
  `registrationId` int(5) NOT NULL COMMENT 'FK to registration',
  PRIMARY KEY (`attendanceId`),
  FOREIGN KEY (`registrationId`) REFERENCES `Registration` (`registrationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
