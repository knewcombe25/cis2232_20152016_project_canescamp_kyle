/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service.impl;

import info.hccis.admin.model.CampActivity;
import info.hccis.canes.dao.repository.CampActivityRepository;
import info.hccis.admin.service.CampActivityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author krystofurr
 */
@Service
public class CampActivityServiceImpl implements CampActivityService {

    public final CampActivityRepository campActivityRepo;
  
    @Autowired
    public CampActivityServiceImpl( CampActivityRepository campActivityRepo ) {
        this.campActivityRepo = campActivityRepo;
    }
    
    @Override
    public List<CampActivity> getCamps(int campID) {
        return campActivityRepo.getCamps(campID);
    }
    
    @Override
    public List<CampActivity> getActivities(int activityID) {
        return campActivityRepo.getActivities(activityID);
    }

}
