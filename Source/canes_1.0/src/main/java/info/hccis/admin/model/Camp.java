/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author krystofurr
 */
@Entity
@Table(name = "Camp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Camp.findAll", query = "SELECT c FROM Camp c")})
public class Camp implements Serializable {
    @OneToMany(mappedBy = "campId")
    private Collection<AppUser> appuserCollection;
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "campId")
    private Integer campId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "descriptionLong")
    private String descriptionLong;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "startDate")
    private String startDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "endDate")
    private String endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "capacity")
    private int capacity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "contactName")
    private String contactName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "camp")
    private Collection<CampActivity> campActivityCollection;

    public Camp() {
    }

    public Camp(Integer campId) {
        this.campId = campId;
    }

    public Camp(Integer campId, String description, String descriptionLong, String startDate, String endDate, int capacity, String contactName) {
        this.campId = campId;
        this.description = description;
        this.descriptionLong = descriptionLong;
        this.startDate = startDate;
        this.endDate = endDate;
        this.capacity = capacity;
        this.contactName = contactName;
    }

    public Integer getCampId() {
        return campId;
    }

    public void setCampId(Integer campId) {
        this.campId = campId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<CampActivity> getCampActivityCollection() {
        return campActivityCollection;
    }

    public void setCampActivityCollection(Collection<CampActivity> campActivityCollection) {
        this.campActivityCollection = campActivityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (campId != null ? campId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Camp)) {
            return false;
        }
        Camp other = (Camp) object;
        if ((this.campId == null && other.campId != null) || (this.campId != null && !this.campId.equals(other.campId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.Camp[ campId=" + campId + " ]";
    }
    
    @XmlTransient
    @JsonIgnore
    public Collection<AppUser> getAppuserCollection() {
        return appuserCollection;
    }

    public void setAppUserCollection(Collection<AppUser> appuserCollection) {
        this.appuserCollection = appuserCollection;
    }
    
}
