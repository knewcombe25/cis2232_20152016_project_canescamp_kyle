/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ecahill109220
 */
@Entity
@Table(name = "Registration")
@XmlRootElement
public class RegistrationSimple implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "registrationId")
    private Integer registrationId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 12)
    @Column(name = "dob")
    private String dob;
    @Column(name = "gradeCode")
    private Integer gradeCode;
    @Column(name = "genderCode")
    private Integer genderCode;
    @Size(max = 50)
    @Column(name = "school")
    private String school;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    @Size(max = 250)
    @Column(name = "otherDescription")
    private String otherDescription;
    @Column(name = "permissionLeaveInd")
    private Integer permissionLeaveInd;
    @Column(name = "permissionPhoto")
    private Integer permissionPhoto;
    @Column(name = "permissionWaiverInd")
    private Integer permissionWaiverInd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "paymentAmount")
    private Double paymentAmount;
    
    @NotNull
    private Integer campId;
    
    @NotNull
    private Integer contactId1;
    
    @NotNull
    private Integer contactId2;
    
    @NotNull
    private Integer contactId3;
    
    @NotNull
    private Integer addressId;

    public RegistrationSimple() {
    }

    public RegistrationSimple(Integer registrationId) {
        this.registrationId = registrationId;
    }

    public RegistrationSimple(Integer registrationId, String name) {
        this.registrationId = registrationId;
        this.name = name;
    }

    public Integer getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getGradeCode() {
        return gradeCode;
    }

    public void setGradeCode(Integer gradeCode) {
        this.gradeCode = gradeCode;
    }

    public Integer getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(Integer genderCode) {
        this.genderCode = genderCode;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtherDescription() {
        return otherDescription;
    }

    public void setOtherDescription(String otherDescription) {
        this.otherDescription = otherDescription;
    }

    public Integer getPermissionLeaveInd() {
        return permissionLeaveInd;
    }

    public void setPermissionLeaveInd(Integer permissionLeaveInd) {
        this.permissionLeaveInd = permissionLeaveInd;
    }

    public Integer getPermissionPhoto() {
        return permissionPhoto;
    }

    public void setPermissionPhoto(Integer permissionPhoto) {
        this.permissionPhoto = permissionPhoto;
    }

    public Integer getPermissionWaiverInd() {
        return permissionWaiverInd;
    }

    public void setPermissionWaiverInd(Integer permissionWaiverInd) {
        this.permissionWaiverInd = permissionWaiverInd;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Integer getCampId() {
        return campId;
    }

    public void setCampId(Integer campId) {
        this.campId = campId;
    }

    public Integer getContactId1() {
        return contactId1;
    }

    public void setContactId1(Integer contactId1) {
        this.contactId1 = contactId1;
    }

    public Integer getContactId2() {
        return contactId2;
    }

    public void setContactId2(Integer contactId2) {
        this.contactId2 = contactId2;
    }

    public Integer getContactId3() {
        return contactId3;
    }

    public void setContactId3(Integer contactId3) {
        this.contactId3 = contactId3;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registrationId != null ? registrationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistrationSimple)) {
            return false;
        }
        RegistrationSimple other = (RegistrationSimple) object;
        if ((this.registrationId == null && other.registrationId != null) || (this.registrationId != null && !this.registrationId.equals(other.registrationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.RegistrationSimple[ registrationId=" + registrationId + " ]";
    }
    
}
