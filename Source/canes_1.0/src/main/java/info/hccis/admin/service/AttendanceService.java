/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.Attendance;
import java.util.List;

/**
 *
 * @author krystofurr
 */
public interface AttendanceService {
    
    public abstract List<Attendance> getAttendances();
    
    public abstract Attendance getAttendance(int id);
    
    public abstract void saveAttendance(Attendance attendance);
    
    public abstract void saveAttendances(List<Attendance> attendances);
    
}
