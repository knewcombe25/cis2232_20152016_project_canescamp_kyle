package info.hccis.admin.web.services;

import info.hccis.admin.model.RegistrationSimple;
import info.hccis.admin.service.RegistrationService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
 
@Component
@Path("/registration")
@Scope("request")
@Produces("application/json")
public class RegistrationRestService {
    
    @Resource
    private final RegistrationService registrationService;
    
    private final static Logger LOGGER = Logger.getLogger(ActivityRestService.class.getName());
    
    public RegistrationRestService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        
        this.registrationService = applicationContext.getBean(RegistrationService.class);
    }
    
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RegistrationSimple> getRegistrationSimples() {
        System.out.println("[ REST SERVICE - RegistrationSimple ]: Getting all registrations");
        
        List<RegistrationSimple> registrations = registrationService.getRegistrations();
        if (registrations == null) {
            LOGGER.severe("No registrations exist");
        } else {
            return registrations;
        }
        
        return null;
    }
    
    @GET
    @Path("/get/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public RegistrationSimple getRegistrationSimple(@PathParam("param") int id) {
        System.out.println("[ REST SERVICE - RegistrationSimple ]: Getting a single registration with ID: " + id);
        
        RegistrationSimple registration = registrationService.getRegistration(id);
        if (registration == null) {
            LOGGER.log(Level.SEVERE, "No registration with ID={0} exists", id);
        } else {
            return registration;
        }
        
        return null;
    }
}
