/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service.impl;

import info.hccis.admin.model.AppUser;
import info.hccis.admin.service.AppUserService;
import info.hccis.canes.dao.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author knewcombe
 */
@Service("appUserService")
public class AppUserImpl implements AppUserService{
    
    public final AppUserRepository appUserRepo;
    
    /**
     *
     * @param appUserRepo
     */
    @Autowired
    public AppUserImpl (AppUserRepository appUserRepo){
        this.appUserRepo = appUserRepo;
    }
    
    @Override
    public AppUser getUser(String username, String password){
        return appUserRepo.getUser(username, password);
    }
    
}
