/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author krystofurr
 */
@Entity
@Table(name = "CampActivities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CampActivity.findAll", query = "SELECT c FROM CampActivity c")})
public class CampActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CampActivityPK campActivityPK;
    @Size(max = 255)
    @Column(name = "startDate")
    private String startDate;
    @Size(max = 255)
    @Column(name = "startTime")
    private String startTime;
    @JoinColumn(name = "activityId", referencedColumnName = "activityId", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Activity activity;
    @JoinColumn(name = "campId", referencedColumnName = "campId", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Camp camp;

    public CampActivity() {
    }

    public CampActivity(CampActivityPK campActivityPK) {
        this.campActivityPK = campActivityPK;
    }

    public CampActivity(int activityId, int campId) {
        this.campActivityPK = new CampActivityPK(activityId, campId);
    }

    public CampActivityPK getCampActivityPK() {
        return campActivityPK;
    }

    public void setCampActivityPK(CampActivityPK campActivityPK) {
        this.campActivityPK = campActivityPK;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Camp getCamp() {
        return camp;
    }

    public void setCamp(Camp camp) {
        this.camp = camp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (campActivityPK != null ? campActivityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampActivity)) {
            return false;
        }
        CampActivity other = (CampActivity) object;
        if ((this.campActivityPK == null && other.campActivityPK != null) || (this.campActivityPK != null && !this.campActivityPK.equals(other.campActivityPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.CampActivity[ campActivityPK=" + campActivityPK + " ]";
    }
    
}
