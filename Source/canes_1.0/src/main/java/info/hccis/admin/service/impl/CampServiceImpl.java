/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service.impl;

import info.hccis.admin.model.Camp;
import info.hccis.canes.dao.repository.CampRepository;
import info.hccis.admin.service.CampService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author krystofurr
 */
@Service
public class CampServiceImpl implements CampService {
    
    public final CampRepository campRepo;
  
    @Autowired
    public CampServiceImpl( CampRepository campRepo ) {
        this.campRepo = campRepo;
    }

    @Override
    public List<Camp> getCamps() {
        return (List)campRepo.findAll();
    }

    @Override
    public Camp getCamp(int id) {
        return campRepo.findOne(id);
    }
    
    
    
}
