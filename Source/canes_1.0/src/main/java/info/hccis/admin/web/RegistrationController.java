package info.hccis.admin.web;

import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.Registration;
import info.hccis.canes.dao.repository.CampRepository;
import info.hccis.canes.dao.repository.RegistrationRepository;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegistrationController {

    private final RegistrationRepository registrationRepository;
    private final CampRepository campRepository;

    @Autowired
    public RegistrationController(RegistrationRepository rr, CampRepository campRepository) {
        this.registrationRepository = rr;
        this.campRepository = campRepository;
    }

    @RequestMapping("/registration/list")
    public String list(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
         System.out.println("Going to list canes camps");
         model.addAttribute("camps", campRepository.findAll());         

        System.out.println("BJM-Going to camp list for registration");
        return "/registration/list";
    }

    @RequestMapping("/registration/add")
    public String enter(Model model, HttpSession session,HttpServletRequest request, DatabaseConnection dbConnectionIn) {
        System.out.println("BJM-Going to registration");
        Registration registration = new Registration();
        registration.setCampId(Integer.parseInt(request.getParameter("id")));
        model.addAttribute("registration", registration);
        return "/registration/add";
    }

    @RequestMapping("/registration/addSubmit")
    public String addSubmit(Model model,  @Valid @ModelAttribute("registration") Registration registration, BindingResult result,HttpSession session, DatabaseConnection dbConnectionIn) {
        if (result.hasErrors()) {
            System.out.println("Error in validation of registration.");
            return "/registration/add";
        }

        
        System.out.println("");
        registrationRepository.save(registration);
        //model.addAttribute("camps", campRepository.findAll());         
        model.addAttribute("registration", new Registration());
        return "/registration/add";
    }
}
