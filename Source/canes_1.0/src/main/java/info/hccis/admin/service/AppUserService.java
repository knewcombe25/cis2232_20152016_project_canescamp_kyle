/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.AppUser;

/**
 *
 * @author knewcombe
 */
public interface AppUserService {
    
    public abstract AppUser getUser(String username, String password);
    
}
