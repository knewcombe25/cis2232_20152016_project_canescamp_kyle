
package info.hccis.admin.service.impl;

import static com.sun.xml.ws.spi.db.BindingContextFactory.LOGGER;
import info.hccis.admin.service.ActivityService;
import info.hccis.canes.dao.repository.ActivityRepository;
import info.hccis.admin.model.Activity;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author krystofurr
 */
@Service("activityService")
public class ActivityServiceImpl implements ActivityService {
    
    public final ActivityRepository activityRepo;
  
    @Autowired
    public ActivityServiceImpl( ActivityRepository activityRepo ) {
        this.activityRepo = activityRepo;
    }

    @Override
    public Activity getActivity(int id) {
        return activityRepo.findOne(id);
    }

    @Override
    public List<Activity> getActivities() {
        return (List)activityRepo.findAll();
    }

    @Override
    public void saveActivity(Activity activity) {
        activityRepo.save(activity);
    }

    @Override
    public void saveActivities(List<Activity> activities) {
            activityRepo.save(activities);
    }

    @Override
    public void deleteActivitiy(int id) {
        activityRepo.delete(id);
    }

    @Override
    public void updateActivityWithSyncStamp(int activityId, String name, String description, String rules, String comments, int rating) {
        activityRepo.updateActivityWithSyncStamp(activityId, name, description, rules, comments, rating);
    }
    
    @Override
    public void updateActivity(int activityId, String name, String description, String rules, String comments, int rating, String syncStamp) {
        activityRepo.updateActivity(activityId, name, description, rules, comments, rating, syncStamp);
    }

    /**
     * Compares two records based on their 'syncStamp'.  Newest record is saved
     * 
     * activityLocal - Mobile record
     * activityRemote - Web service record
     * 
     * This method upon completion will either return null meaning that the mobile versions record
     * was the newest and that the web service saved it OR the web service record is retured to the mobile
     * application to save in it's local database.
     * 
     * @param activityLocal 
     * @return  
     */
    @Override
    public Activity syncActivity(Activity activityLocal) {

        Activity returnResult = null;
        Date syncStampRemote, syncStampLocal;
        syncStampRemote = syncStampLocal = null;
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat formatter = new SimpleDateFormat(dateFormat);
        
        
        // Get the corresponding activity in the remote database
        Activity activityRemote = activityRepo.findOne(activityLocal.getActivityId());
        
        try {
            syncStampRemote = formatter.parse(activityRemote.getSyncstamp());
            syncStampLocal = formatter.parse(activityLocal.getSyncstamp());
        } catch(Exception ex) {
            LOGGER.log(Level.SEVERE, "Failed to parse syncstamps into date objects.{0}", ex.getMessage());
        }

        
        System.out.println("LOCAL RECORD: " + activityLocal.getSyncstamp());
        System.out.println("REMOTE RECORD: " + activityRemote.getSyncstamp());

        if(syncStampLocal.after(syncStampRemote)){
            
//            LOCAL: 2016-02-11 19:42:53
//            REMOTE: 2016-02-10 11:07:36.0
//            Remote is newer
        
            System.out.println("Local is newer");
            activityRepo.updateActivity(activityLocal.getActivityId(), activityLocal.getName(),
                                        activityLocal.getDescription(), activityLocal.getRules(), 
                                        activityLocal.getComments(), activityLocal.getRating(),
                                        activityLocal.getSyncstamp());
            
            // No need to set returnResult.  It is already 'null' and will return
            
        } else {
            
            // No need to complete an update if the remote record is the newest.  It already exists
            System.out.println("Remote is newer");
            // Send back the remote record to the mobile application
            returnResult = activityRemote;
        }
        
        return returnResult;
    }

    /**
     * Deletes an activity based on it's ID
     * 
     * @param activityId 
     */
    @Override
    public void deleteActivity(int activityId) {
        activityRepo.delete(activityId);
    }
    
    
}
