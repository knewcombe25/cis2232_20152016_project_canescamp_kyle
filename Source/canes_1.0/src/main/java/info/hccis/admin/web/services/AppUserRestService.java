/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.web.services;

import static com.sun.xml.ws.spi.db.BindingContextFactory.LOGGER;
import info.hccis.admin.model.AppUser;
import info.hccis.admin.service.AppUserService;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author knewcombe
 */

@Component
@Path("/appuser")
@Scope("request") // THIS WILL CAUSE SERVLET TO CRASH IF COMMENTED
@Produces("application/json")
public class AppUserRestService {
    
    @Resource
    private final AppUserService appUserService;
    
    public AppUserRestService(@Context ServletContext servletContext){
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        
        this.appUserService = applicationContext.getBean(AppUserService.class);
        
    }
    
    @GET
    @Path("/get/user/{username}/{password}")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Produces(MediaType.APPLICATION_JSON)
    public AppUser getUserByUsername(@PathParam("username") String username, @PathParam("password") String password) {
    
        System.out.println("[ REST SERVICE - AppUser ]: Get user with matching username and passwords");
//        List<CampActivity> campActivity = campActivityService.getCamps(campID);
        AppUser appUser = appUserService.getUser(username, password);
        if(appUser == null) {
            LOGGER.severe("This camp does not exist");
        } else {
            return appUser;
        }
        return null;
    }
    
}
