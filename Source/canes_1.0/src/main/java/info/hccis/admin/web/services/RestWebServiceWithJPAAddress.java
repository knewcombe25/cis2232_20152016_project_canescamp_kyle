package info.hccis.admin.web.services;

import info.hccis.canes.dao.repository.AddressRepository;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

//issues here...
//http://www.scriptscoop.net/t/ba4553fde8ca/spring-autowired-classes-are-null-in-jersey-rest.html
@Component
@Path("/address")
@Scope("request")
public class RestWebServiceWithJPAAddress {

    @Resource
    private final AddressRepository ar;

    public RestWebServiceWithJPAAddress(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.ar = applicationContext.getBean(AddressRepository.class);
    }

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String theId) {

        String output = "error";
        System.out.println("in Jersey hello web service");

        boolean validId = false;
        int id = 0;
        try {
            id = Integer.parseInt(theId);
            validId = true;
        } catch (Exception e) {
            id = 1;
            return Response.status(400).entity(output).build();
        }

        try {

            System.out.println("getting to here, id=" + id);
            if (validId) {
                output = ar.findOne(id).toString();
            }
        } catch (Exception e) {
            return Response.status(204).entity("no data found").build();
        }
        return Response.status(200).entity(output).build();

    }

}
