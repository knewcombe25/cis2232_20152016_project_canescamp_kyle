
package info.hccis.admin.service;

import info.hccis.admin.model.Activity;
import java.util.List;

/**
 *
 * @author krystofurr
 */

public interface ActivityService {
    
    // Get a single activity
    public abstract Activity getActivity(int id);

    // Get all activities
    public abstract List<Activity> getActivities();
    
    // Save a single activity
    public abstract void saveActivity(Activity activity);
    
    // Save all activities
    public abstract void saveActivities(List<Activity> activities);
    
    // Delete an activity
    public abstract void deleteActivitiy(int id);
    
    // Update an activity including automated timestamp
    public abstract void updateActivityWithSyncStamp(int activityId, String name, String description, String rules, String comments, int rating);
    
    // Update an activity
    public abstract void updateActivity(int activityId, String name, String description, String rules, String comments, int rating, String syncStamp);
    
    // Delete an activity
    public abstract void deleteActivity(int activityId);

    // Sync activity
    public abstract Activity syncActivity(Activity activity);
    
    
}
