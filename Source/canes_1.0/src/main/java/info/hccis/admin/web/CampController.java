package info.hccis.admin.web;

import info.hccis.admin.model.Camp;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.canes.dao.repository.CampRepository;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CampController {

    private final CampRepository campRepository;

    @Autowired
    public CampController(CampRepository campRepository) {
        this.campRepository = campRepository;
    }
    
    @RequestMapping("/camp/add")
    public String addCamp(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
         System.out.println("Going to add canes camp");
         model.addAttribute("camp", new Camp());
        return "/camp/add";
    }        

    
    @RequestMapping("/camp/edit")
    public String editCamp(Model model, HttpServletRequest request, DatabaseConnection dbConnectionIn) {
         System.out.println("Going to edit canes camp");
         model.addAttribute("camp", campRepository.findOne(Integer.parseInt(request.getParameter("id"))));
        return "/camp/add";
    }        

    
    @RequestMapping("/camp/list")
    public String list(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
         System.out.println("Going to list canes camps");
         model.addAttribute("camps", campRepository.findAll());         
        return "/camp/list";
    }        

    
    @RequestMapping("/camp/addSubmit")
    public String addCampSubmit(Model model, Camp camp, HttpSession session, DatabaseConnection dbConnectionIn) {
         System.out.println("Going back to add canes camp");
         campRepository.save(camp);
         model.addAttribute("camps", campRepository.findAll());         
        return "/camp/list";
    }        
    
}
