package info.hccis.canes.dao.repository;

import info.hccis.admin.model.RegistrationSimple;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegistrationSimpleRepository extends CrudRepository<RegistrationSimple, Integer> {

}